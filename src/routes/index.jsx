import HomeComponent from '../view/Home';

var indexRoutes = [
    { path: "/", name: "Home", component: HomeComponent }

];

export default indexRoutes;
