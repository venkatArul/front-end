export const Service={
    getAssetList,
    viewOrDeleteLogEntry
};

const url='http://localhost:5000/asset'

function getAssetList(custId){
    return fetch(`${url}/getMsgsOfaParticularCustomer/${custId}`).then(function(response){
            return response.json();
        });          
}

function viewOrDeleteLogEntry(data){    
    return fetch(`${url}/updateLogSeenStatusOrDeletionByInsertion`,{
          method: 'POST',
          headers: {
            "Content-Type": "application/json; charset=utf-8",
        },
          body: JSON.stringify(data),
        }).then(function(response){
            return response.json();
        });
}