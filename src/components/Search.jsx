import React from 'react';
import '../asserts/css/Search.css';

class Search extends React.Component{
   

    onDataChange = (event) =>{
        this.props.onChange(event.target.value);
    }

    onButtonClick = () =>{
        this.props.onClick();
    }

    render(){
        return(
        <div class="wrap">
            <input onChange={this.onDataChange} type="text" className="searchTerm" placeholder="Search"/>
            <button onClick={this.onButtonClick} title="search" className="searchButton"> <i class="fas fa-search"></i> </button>
        </div>
        )
    }
}

export default Search;