import React from 'react';
import { Service } from '../service/Service';

class Result extends React.Component{
    constructor(props) {
        super(props);
        this.viewMessage = this.viewMessage.bind(this);
        this.deleteMessage = this.deleteMessage.bind(this);
      }

    viewMessage(data){
       Service.viewOrDeleteLogEntry({assetId:data.assetId,custId:data.custId,updateType:'seen'}).then((response) => {
        if (response.success) {
          console.log('View successfully');
        } else {
            console.log('Log entry failed');
        }

      });
    }

    deleteMessage(data){
        Service.viewOrDeleteLogEntry({assetId:data.assetId,custId:data.custId,updateType:'delete'}).then((response) => {
            if (response.success) {
                console.log('deleted successfully');
            } else {
                console.log('deletion failed');
            }
      
          });
    }

    render(){
        return(
        <div className="row">
        {this.props.data.map((value, index) =>
            <div className="col-md-6" key={value.toString()}>
             <div class="card">
                <div class="card-header">{value.title}</div>
                <div class="card-body">
                description:{value.description}<br/>
                HTML Data:{value.htmlString}<br/>
                id:{value.assetId}<br/>
                </div> 
                <div class="card-footer">
               <button onClick={this.viewMessage(value)}>View Message</button>
               <button onClick={this.deleteMessage(value)}>Delete Message</button>
                }
                </div>
            </div>  
            </div>)
        }
        </div> 
        )
    }
}

export default Result;
