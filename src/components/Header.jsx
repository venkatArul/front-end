import React from 'react';
import logo from '../logo.jpg';


class Header extends React.Component{
    render(){
        return(
        <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Customer Asset List</h1>
        </header>
        </div>
        )
    }
}
export default Header;