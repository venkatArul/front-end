import React from 'react';
import '../App.css'
import Header from '../components/Header';
import Search from '../components/Search';
import Result from '../components/Result';
import Nodata from '../components/Nodata';
import { Service } from '../service/Service';
let custId;


class HomeComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {messageDataArray:[],nodata:true};
  }
  componentDidMount() {

  }

  //onInputChange is called when the data in text box is changed
  onInputChange = (data) => {
    custId = data;
  }

  //onSearchClick is called when the search button is clicked
  onSearchClick = () => {
    Service.getAssetList(custId).then((response) => {
      if (response.length) {
        this.setState({ messageDataArray: response,nodata:false })
      } else {
        this.setState({ nodata: true, messageDataArray: []})
      }

    });

  }

  render() {
    return (
      <React.Fragment>
        <Header />
        <Search onChange={this.onInputChange} onClick={this.onSearchClick} />
        {this.state.nodata ? <Nodata /> :
          <Result data={this.state.messageDataArray} />}
      </React.Fragment>
    );
  }
}

export default HomeComponent;
